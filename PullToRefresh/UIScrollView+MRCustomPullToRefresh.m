//
//  UIScrollView+CustomPulToRefresh.m
//  CustomPullToRefresh
//
//  Created by Хужаяров Марат on 30/04/15.
//  Copyright (c) 2015 Marat Khuzhayarov. All rights reserved.
//

#import "UIScrollView+MRCustomPullToRefresh.h"

#import <objc/runtime.h>

static CGFloat const PullToRefreshViewDefaultHeight = 60;

static char UIScrollViewTopPullToRefreshView;

static char UIScrollViewBottomPullToRefreshView;

#pragma mark -
#pragma mark - CustomPullToRefreshView Interface
#pragma mark -


@interface MRPullToRefreshView ()

@property (nonatomic, assign) UIEdgeInsets originalInset;

@property (nonatomic, assign) MRPullToRefreshPosition position;

@end


#pragma mark -
#pragma mark - UIScrollView (CustomPulToRefresh) Implementation
#pragma mark -


@implementation UIScrollView (MRCustomPullToRefresh)

@dynamic topPullToRefreshView;

- (void)addPullToRefreshWithActionHandler:(void (^)(void))actionHandler position:(MRPullToRefreshPosition)position {
    
    MRPullToRefreshView *view = [[MRPullToRefreshView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, PullToRefreshViewDefaultHeight)];
    view.backgroundColor = [UIColor redColor];
    view.pullToRefreshActionHandler = actionHandler;
    
    [self addPullToRefreshView:view position:position];
}


- (void)addPullToRefreshView:(MRPullToRefreshView *)pullTorefreshView position:(MRPullToRefreshPosition)position {
    [self.topPullToRefreshView removeFromSuperview];
    
    pullTorefreshView.scrollView = self;
    [self insertSubview:pullTorefreshView atIndex:0];
    
    CGFloat yOrigin;
    switch (position) {
        case MRPullToRefreshPositionTop:
            yOrigin = -PullToRefreshViewDefaultHeight;
            self.topPullToRefreshView = pullTorefreshView;
            break;
        case MRPullToRefreshPositionBottom:
            yOrigin = self.contentSize.height;
            self.bottomPullToRefreshView = pullTorefreshView;
            break;
        default:
            return;
    }
    CGRect frame = pullTorefreshView.frame;
    frame.origin.y = yOrigin;
    pullTorefreshView.frame = frame;
    pullTorefreshView.originalInset = self.contentInset;
    pullTorefreshView.position = position;
    //        self.showsPullToRefresh = YES;
    
}


- (MRPullToRefreshView *)topPullToRefreshView {
    return objc_getAssociatedObject(self, &UIScrollViewTopPullToRefreshView);
}


- (void)setTopPullToRefreshView:(UIView *)pullToRefreshView {
    if (self.topPullToRefreshView) {
        [self removeObserver:self.topPullToRefreshView forKeyPath:@"contentOffset"];
        [self removeObserver:self.topPullToRefreshView forKeyPath:@"contentSize"];
        [self removeObserver:self.topPullToRefreshView forKeyPath:@"frame"];
    }
    
    objc_setAssociatedObject(self, &UIScrollViewTopPullToRefreshView,
                             pullToRefreshView,
                             OBJC_ASSOCIATION_ASSIGN);
    
    [self addObserver:pullToRefreshView forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:pullToRefreshView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:pullToRefreshView forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

- (MRPullToRefreshView *)bottomPullToRefreshView {
    return objc_getAssociatedObject(self, &UIScrollViewBottomPullToRefreshView);
}

- (void)setBottomPullToRefreshView:(UIView *)pullToRefreshView {
    if (self.bottomPullToRefreshView) {
        [self removeObserver:self.bottomPullToRefreshView forKeyPath:@"contentOffset"];
        [self removeObserver:self.bottomPullToRefreshView forKeyPath:@"contentSize"];
        [self removeObserver:self.bottomPullToRefreshView forKeyPath:@"frame"];
    }
    
    objc_setAssociatedObject(self, &UIScrollViewBottomPullToRefreshView,
                             pullToRefreshView,
                             OBJC_ASSOCIATION_ASSIGN);
    
    [self addObserver:self.bottomPullToRefreshView forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self.bottomPullToRefreshView forKeyPath:@"contentSize" options:NSKeyValueObservingOptionNew context:nil];
    [self addObserver:self.bottomPullToRefreshView forKeyPath:@"frame" options:NSKeyValueObservingOptionNew context:nil];
}

@end





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark -
#pragma mark - CustomPullToRefreshView Implementation
#pragma mark -


@implementation MRPullToRefreshView


@synthesize currentState = _state;


- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _triggerAreaHeight = frame.size.height;
        _state = MRPullToRefreshStateStopped;
        self.clipsToBounds = YES;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UIScrollView *scrollView = self.scrollView;
    CGRect fr = self.frame;
    fr.size.width = scrollView.frame.size.width;
    
    if (self.position == MRPullToRefreshPositionBottom) {
        fr.origin.y = scrollView.contentSize.height < scrollView.frame.size.height ? scrollView.frame.size.height : scrollView.contentSize.height;
    }
    
    self.frame = fr;
    
    NSLog(@"layoutSubviews:%@",NSStringFromCGRect(scrollView.frame));
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (self.superview && newSuperview == nil) {
        UIScrollView *scrollView = (UIScrollView *)self.superview;
        [scrollView removeObserver:self forKeyPath:@"contentOffset"];
        [scrollView removeObserver:self forKeyPath:@"contentSize"];
        [scrollView removeObserver:self forKeyPath:@"frame"];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"contentOffset"]) {
        [self scrollViewDidScroll:[[change valueForKey:NSKeyValueChangeNewKey] CGPointValue]];
    }
    else if([keyPath isEqualToString:@"contentSize"]) {
        [self layoutSubviews];
    }
    else if([keyPath isEqualToString:@"frame"]) {
        [self layoutSubviews];
    }
    
}

- (void)scrollViewDidScroll:(CGPoint)contentOffset {
    if (self.currentState != MRPullToRefreshStateLoading) {
        
        //CGPoint p = [self.scrollView convertPoint:<#(CGPoint)#> fromView:<#(UIView *)#>]
        
        CGRect fr = self.frame;
        fr.origin.y = contentOffset.y;
        self.frame = fr;
        
        CGFloat scrollOffsetThreshold = 0;
        CGFloat yOffset = 0;
        BOOL triggerHeightFlag = NO;
        
        if (self.position == MRPullToRefreshPositionTop) {
            scrollOffsetThreshold = fabs(self.frame.origin.y - self.originalInset.top);
            yOffset = -contentOffset.y;
            triggerHeightFlag = yOffset > self.triggerAreaHeight;
        }
        else if (self.position == MRPullToRefreshPositionBottom) {
            scrollOffsetThreshold = fabs(self.scrollView.contentSize.height - self.scrollView.frame.size.height);
            yOffset = contentOffset.y - scrollOffsetThreshold;
            triggerHeightFlag = contentOffset.y > scrollOffsetThreshold + self.triggerAreaHeight;
        }
        if (!self.scrollView.isDragging && self.currentState == MRPullToRefreshStateTriggered) {
            self.currentState = MRPullToRefreshStateLoading;
        }
        else if (triggerHeightFlag && !self.scrollView.dragging && self.currentState == MRPullToRefreshStateStopped) {
            self.currentState = MRPullToRefreshStateTriggered;
        }
        else if (self.scrollView.isDragging && self.currentState == MRPullToRefreshStateStopped) {
            [self contentOffsetYChanged:yOffset];
        }
    }
}

- (void)contentOffsetYChanged:(CGFloat)contentOffset {
    
}


#pragma mark -

- (void)setCurrentState:(MRPullToRefreshState)newState {
    
    if (_state == newState)
        return;
    
    MRPullToRefreshState previousState = _state;
    _state = newState;
    
    [self setNeedsLayout];
    
    switch (newState) {
        case MRPullToRefreshStateStopped:
            [self resetScrollViewContentInset];
            break;
            
        case MRPullToRefreshStateTriggered:
            [self startAnimating];
            break;
            
        case MRPullToRefreshStateLoading:
            [self setScrollViewContentInsetForLoading];
            
            if (previousState == MRPullToRefreshStateTriggered && self.pullToRefreshActionHandler != NULL) {
                self.pullToRefreshActionHandler();
            }
            break;
            
        default: break;
    }
}

- (void)startAnimating {
    self.currentState = MRPullToRefreshStateLoading;
}

- (void)stopAnimating {
    self.currentState = MRPullToRefreshStateStopped;
}

#pragma mark - Scroll View

- (void)resetScrollViewContentInset {
    [self setScrollViewContentInset:self.originalInset];
}

- (void)setScrollViewContentInsetForLoading {
    UIEdgeInsets currentInsets = self.scrollView.contentInset;
    if (self.position == MRPullToRefreshPositionTop) {
        currentInsets.top = self.frame.size.height;
    }
    else if (self.position == MRPullToRefreshPositionBottom) {
        currentInsets.bottom = self.frame.size.height;
    }
    [self setScrollViewContentInset:currentInsets];
}

- (void)setScrollViewContentInset:(UIEdgeInsets)contentInset {
    [self.scrollView setContentOffset:self.scrollView.contentOffset animated:NO];
    
    [UIView animateWithDuration:.3f
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction|UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionAllowAnimatedContent
                     animations:^{
                         self.scrollView.contentInset = contentInset;
                     }
                     completion:NULL];
}

@end
