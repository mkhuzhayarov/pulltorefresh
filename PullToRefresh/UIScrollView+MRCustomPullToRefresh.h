//
//  UIScrollView+CustomPulToRefresh.h
//  CustomPullToRefresh
//
//  Created by Хужаяров Марат on 30/04/15.
//  Copyright (c) 2015 Marat Khuzhayarov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PullToRefreshActionHandler)();

typedef enum {
    MRPullToRefreshStateStopped = 0,
    MRPullToRefreshStateTriggered,
    MRPullToRefreshStateLoading
} MRPullToRefreshState;

typedef NS_ENUM(NSUInteger, MRPullToRefreshPosition) {
    MRPullToRefreshPositionTop = 0,
    MRPullToRefreshPositionBottom,
};


#pragma mark -
#pragma mark - CustomPullToRefreshView Interface
#pragma mark -


@interface MRPullToRefreshView : UIView

/**
 * Parent scrollView(it can be any subclass of UIScrollView)
 */
@property (nonatomic, weak) UIScrollView *scrollView;

@property (nonatomic, assign) CGFloat triggerAreaHeight;

@property (nonatomic, readonly) MRPullToRefreshState currentState;

@property (nonatomic, copy) PullToRefreshActionHandler pullToRefreshActionHandler;

- (void)startAnimating;

- (void)stopAnimating;

- (void)contentOffsetYChanged:(CGFloat)contentOffset;

@end



#pragma mark -
#pragma mark - UIScrollView(CustomPulToRefresh) Interface
#pragma mark -



@interface UIScrollView (MRCustomPullToRefresh)

/**
 * PullToRefreshView with postion:MRPullToRefreshPositionTop
 */
@property (nonatomic, strong, readonly) MRPullToRefreshView *topPullToRefreshView;

/**
 * PullToRefreshView with postion:MRPullToRefreshPositionBottom
 */
@property (nonatomic, strong, readonly) MRPullToRefreshView *bottomPullToRefreshView;

/**
 * Add default pullTorefreshView with actionHandler to UIScrollView with postion
 *
 * @param actionHandler - The handler block to execute.
 * @param position - top or bottom
 */
- (void)addPullToRefreshWithActionHandler:(void (^)(void))actionHandler position:(MRPullToRefreshPosition)position;

/**
 * Add custom pullTorefreshView to UIScrollView with postion
 *
 * @param pullTorefreshView - The refreshView to be added.
 * @param position - top or bottom
 */
- (void)addPullToRefreshView:(MRPullToRefreshView *)pullTorefreshView position:(MRPullToRefreshPosition)position;

@end
